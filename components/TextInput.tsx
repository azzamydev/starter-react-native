import clsx from 'clsx'
import React from 'react'
import {
    Text,
    TextInput as TextInputBase,
    TextInputProps as TextInputBaseProps,
    View
} from 'react-native'

interface TextInputProps extends TextInputBaseProps {
    label?: string
    handleChangeText?: (e: string | undefined) => void
    containerStyle?: string
    textFieldStyle?: string
    error?: string | null
}

const TextInput = ({
    label,
    handleChangeText,
    containerStyle,
    textFieldStyle,
    error,
    ...props
}: TextInputProps) => {
    return (
        <View className={clsx('', containerStyle)}>
            <Text className="text-base mb-1">{label ?? 'Label'}</Text>

            <View className="relative">
                <TextInputBase
                    {...props}
                    style={{
                        fontSize: 14,
                        ...(props.multiline && {
                            textAlignVertical: 'top',
                            height: 100,
                            paddingVertical: 12
                        })
                    }}
                    placeholder={props.placeholder ?? 'Masukan disini'}
                    placeholderTextColor={'gray'}
                    className={clsx(
                        'w-full px-4 rounded-lg h-12 bg-slate-50 border border-gray-300 focus:border-primary',
                        textFieldStyle
                    )}
                    onChangeText={handleChangeText}
                />
            </View>
            {error && (
                <Text className="text-xs ml-1 mt-1 text-red-500">{error}</Text>
            )}
        </View>
    )
}

export default TextInput
