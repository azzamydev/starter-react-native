import clsx from 'clsx'
import React from 'react'
import { Platform, StyleSheet, Text, View } from 'react-native'
import { PickerSelectProps } from 'react-native-picker-select'
import RNPickerSelect from 'react-native-picker-select'

interface DropdownProps extends PickerSelectProps {
    label: string
    containerStyle?: string
}

const Dropdown = ({
    onValueChange,
    items,
    label,
    containerStyle,
    ...props
}: DropdownProps) => {
    const isIos = Platform.OS === 'ios'

    return (
        <View className={clsx('', containerStyle)}>
            <Text className="text-base mb-1">{label ?? 'Label'}</Text>
            <View
                className={clsx(
                    'border rounded-lg border-gray-300 h-12 items-center flex-row flex bg-slate-50 focus:border-primary',
                    isIos && 'px-3',
                    containerStyle
                )}>
                <RNPickerSelect
                    {...props}
                    style={{
                        placeholder: {
                            color: 'gray',
                            fontWeight: 500
                        },
                        viewContainer: {
                            flex: 1,
                            justifyContent: 'center'
                        }
                    }}
                    onValueChange={onValueChange}
                    items={items}
                />
            </View>
        </View>
    )
}

export default Dropdown

const styles = StyleSheet.create({})
