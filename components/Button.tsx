import clsx from 'clsx'
import React, { forwardRef } from 'react'
import { Text, Pressable, PressableProps } from 'react-native'
import { styled } from 'nativewind'

const StyledPressable = styled(Pressable)
const StyledText = styled(Text)

interface ButtonProps extends PressableProps {
    onPress?: () => void
    title: string
    containerStyle?: string
    textStyle?: string
}

const Button = forwardRef<any, ButtonProps>(
    ({ onPress, title, containerStyle, textStyle, disabled }, ref) => {
        return (
            <StyledPressable
                ref={ref}
                disabled={disabled}
                onPress={onPress}
                className={clsx(
                    'w-full bg-primary-600 justify-center items-center px-3 py-4 rounded-xl active:bg-primary-700 active:scale-95 duration-300',
                    containerStyle
                )}>
                <StyledText className={clsx('text-white text-base', textStyle)}>
                    {title}
                </StyledText>
            </StyledPressable>
        )
    }
)

export default Button
