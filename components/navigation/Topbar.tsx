import { logo } from '@/constants/Images'
import React from 'react'
import { Image, Platform, Text, View } from 'react-native'

const TopBar = () => {
    const isAndroid = Platform.OS === 'android'
    const isIos = Platform.OS === 'ios'

    return (
        <View
            className={`bg-white w-full px-4 sticky top-0 border-b border-gray-200 pb-3 h-fit ${
                isAndroid && 'pt-12'
            } ${isIos && 'pt-[44px]'}`}>
            <View className="flex-row items-center justify-between">
                <Image
                    source={logo}
                    resizeMode="contain"
                    className="w-[45px] h-[45px]"
                    tintColor={'#3b76d5'}
                />
                <View className="gap-3 flex-row items-center">
                    <Text>Admin</Text>
                </View>
            </View>
        </View>
    )
}

export default TopBar
