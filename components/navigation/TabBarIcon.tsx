// You can explore the built-in icon families and icons on the web at https://icons.expo.fyi/

import {
    ColorValue,
    Image,
    ImageSourcePropType,
    Text,
    View
} from 'react-native'

interface TabBarIconProps {
    icon: ImageSourcePropType
    color: ColorValue
    name: string
    focused: boolean
}

export function TabBarIcon({ icon, color, name, focused }: TabBarIconProps) {
    return (
        <View className="items-center min-w-[100px] justify-center gap-2 pt-2">
            <Image
                source={icon}
                resizeMode="contain"
                tintColor={color}
                className="w-6 h-6"
            />
            <Text
                className={`${
                    focused
                        ? `font-bold text-primary-600`
                        : 'font-normal text-gray-500'
                } text-xs`}>
                {name}
            </Text>
        </View>
    )
}
