import { Ionicons } from '@expo/vector-icons'
import clsx from 'clsx'
import React, { useState } from 'react'
import {
    Pressable,
    Text,
    TextInput as TextInputBase,
    TextInputProps as TextInputBaseProps,
    View
} from 'react-native'

interface TextInputProps extends TextInputBaseProps {
    label?: string
    handleChangeText?: (e: string | undefined) => void
    containerStyle?: string
    textFieldStyle?: string
    error?: string | null
}

const TextInputPassword = ({
    label,
    handleChangeText,
    containerStyle,
    textFieldStyle,
    error,
    ...props
}: TextInputProps) => {
    const [showPassword, setShowPassword] = useState(false)

    return (
        <View className={clsx('', containerStyle)}>
            <Text className="text-base mb-1">{label ?? 'Label'}</Text>

            <View className="relative">
                <View className="absolute z-10 right-4 top-3.5 inset-y-0 flex-1 justify-center items-center">
                    <Pressable
                        onPress={() => setShowPassword((state) => !state)}>
                        <Ionicons
                            size={22}
                            name={showPassword ? 'eye-off' : 'eye'}
                            color={'gray'}
                        />
                    </Pressable>
                </View>
                <TextInputBase
                    {...props}
                    style={{
                        fontSize: 14,
                        ...(props.multiline && {
                            textAlignVertical: 'top',
                            height: 100,
                            paddingVertical: 12
                        })
                    }}
                    placeholder={props.placeholder ?? 'Masukan disini'}
                    placeholderTextColor={'gray'}
                    className={clsx(
                        'w-full px-4 rounded-lg h-12 bg-slate-50 border border-gray-300 focus:border-primary',
                        textFieldStyle
                    )}
                    secureTextEntry={!showPassword}
                    onChangeText={handleChangeText}
                />
            </View>
            {error && (
                <Text className="text-xs ml-1 mt-1 text-red-500">{error}</Text>
            )}
        </View>
    )
}

export default TextInputPassword
