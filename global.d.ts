/// <reference types="nativewind/types" />
declare module '*.png'
declare module '*.svg'
declare module '*.jpeg'
declare module '*.jpg'

declare module '@env' {
    export const API_BASE: string
}
