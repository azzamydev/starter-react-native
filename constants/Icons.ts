import home from '../assets/icons/home.png'
import profile from '../assets/icons/profile.png'
import logout from '../assets/icons/logout.png'
import headphone from '../assets/icons/headphone.png'
import pencil from '../assets/icons/pencil.png'

export { home, profile, logout, headphone, pencil }
