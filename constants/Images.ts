import logo from '../assets/images/logo.png'
import person from '../assets/images/person.png'
import icon from '../assets/images/icon.png'

export { logo, person, icon }
