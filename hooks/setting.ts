import AsyncStorage from '@react-native-async-storage/async-storage'
import { create } from 'zustand'
import { createJSONStorage, persist } from 'zustand/middleware'

interface SettingState {
    onBoarding: boolean
    appStarted: () => void
    resetBoarding: () => void
}

export const useSetting = create<SettingState>()(
    persist(
        (set, get) => ({
            // initial state
            onBoarding: false,
            appStarted: async () => {
                set(() => ({
                    onBoarding: true
                }))
            },
            resetBoarding: async () => {
                set(() => ({
                    onBoarding: false
                }))
            }
        }),
        {
            name: 'setting',
            storage: createJSONStorage(() => AsyncStorage)
        }
    )
)
