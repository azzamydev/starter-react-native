/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './app/**/*.{js,jsx,ts,tsx}',
        './components/**/*.{js,jsx,ts,tsx}'
    ],
    theme: {
        extend: {
            colors: {
                primary: {
                    50: '#f1f7fd',
                    100: '#dfedfa',
                    200: '#c6dff7',
                    300: '#9fccf1',
                    400: '#71afe9',
                    500: '#5091e1',
                    600: '#3b76d5',
                    700: '#3262c3',
                    800: '#3054a6',
                    900: '#2a467e',
                    950: '#1e2c4d'
                }
            }
        }
    },
    plugins: []
}
