import { useNavigation } from 'expo-router'
import React, { useEffect } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'

const Profile = () => {
    const navigation = useNavigation()

    useEffect(() => {
        navigation.setOptions({
            title: 'Profile',
            headerBackTitle: 'Back'
        })
    }, [navigation])
    return (
        <ScrollView
            className="h-full bg-white"
            contentContainerStyle={{
                flex: 1
            }}>
            <View className="items-center justify-center flex-1">
                <Text>My Profile</Text>
            </View>
        </ScrollView>
    )
}

export default Profile

const styles = StyleSheet.create({})
