import { StatusBar } from 'expo-status-bar'
import { useState } from 'react'
import {
    RefreshControl,
    SafeAreaView,
    ScrollView,
    Text,
    View
} from 'react-native'

export default function HomeScreen() {
    const [refreshing, setRefreshing] = useState(false)

    const onRefresh = () => {
        setRefreshing(true)
        /* Do Something */
        setRefreshing(false)
    }
    return (
        <SafeAreaView className="bg-white">
            <ScrollView
                className="h-full"
                contentContainerStyle={{
                    flex: 1
                }}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }>
                <View className="items-center justify-center flex-1">
                    <Text>Welcome to home</Text>
                </View>
            </ScrollView>
            <StatusBar style="dark" />
        </SafeAreaView>
    )
}
