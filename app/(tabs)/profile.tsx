import { headphone, logout as logoutImage, pencil } from '@/constants/Icons'
import { person } from '@/constants/Images'
import { useRouter } from 'expo-router'
import { StatusBar } from 'expo-status-bar'
import React, { useEffect, useState } from 'react'
import {
    Alert,
    Image,
    Pressable,
    RefreshControl,
    SafeAreaView,
    ScrollView,
    Text,
    View
} from 'react-native'

const Profile = () => {
    const [refreshing, setRefreshing] = useState(false)
    const router = useRouter()
    const doLogout = () => {
        Alert.alert('Logout', 'Apakah Anda yakin?', [
            {
                text: 'Batal',
                style: 'cancel'
            },
            {
                text: 'Ya',
                onPress: () => {
                    router.replace('/')
                }
            }
        ])
    }

    const onRefresh = () => {
        setRefreshing(true)
        /* Do Something */
        setRefreshing(false)
    }

    useEffect(() => {
        /* Do Something */
    }, [])

    return (
        <SafeAreaView className="bg-white">
            <ScrollView
                className="h-full"
                contentContainerStyle={{
                    flex: 1
                }}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }>
                <View className="h-full px-4">
                    <View className="items-center justify-center py-10 flex-1">
                        <Image
                            source={person}
                            resizeMode="contain"
                            className="w-[70px] h-[70px] mb-5"
                            tintColor={'#3b76d5'}
                        />
                        <Text className="text-xl">Admin</Text>
                    </View>

                    <View className="border  border-gray-200 divide-y divide-gray-200 rounded-lg overflow-hidden">
                        <Pressable
                            onPress={() => router.push('profile')}
                            className="p-4 active:bg-gray-100 flex-row space-x-3 items-center">
                            <Image
                                source={pencil}
                                resizeMode="contain"
                                className="w-5 h-5"
                            />
                            <Text>Profile</Text>
                        </Pressable>
                        <Pressable className="p-4 active:bg-gray-100 flex-row space-x-3 items-center">
                            <Image
                                source={headphone}
                                resizeMode="contain"
                                className="w-5 h-5"
                            />
                            <Text>Bantuan</Text>
                        </Pressable>
                        <Pressable
                            onPress={doLogout}
                            className="p-4 active:bg-gray-100 flex-row space-x-3 items-center">
                            <Image
                                source={logoutImage}
                                resizeMode="contain"
                                className="w-5 h-5"
                                tintColor={'#FF0000'}
                            />
                            <Text className="text-red-500">Logout</Text>
                        </Pressable>
                    </View>
                    <View className="h-[100px] items-center justify-center">
                        <Text className="text-gray-500">version 0.1</Text>
                    </View>
                </View>
            </ScrollView>
            <StatusBar style="dark" />
        </SafeAreaView>
    )
}

export default Profile
