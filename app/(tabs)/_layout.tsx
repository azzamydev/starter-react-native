import { Tabs } from 'expo-router'
import { TabBarIcon } from '@/components/navigation/TabBarIcon'
import TopBar from '@/components/navigation/Topbar'
import { Platform, View } from 'react-native'
import { home, profile } from '@/constants/Icons'

export default function TabLayout() {
    return (
        <Tabs
            screenOptions={{
                tabBarShowLabel: false,
                tabBarStyle: {
                    height: Platform.OS === 'ios' ? 100 : 75
                },
                tabBarActiveTintColor: '#3b76d5',
                header: () => <TopBar />
            }}>
            <Tabs.Screen
                name="index"
                options={{
                    title: 'Home',
                    tabBarIcon: ({ color, focused }) => (
                        <TabBarIcon
                            icon={home}
                            name="Home"
                            focused={focused}
                            color={color}
                        />
                    )
                }}
            />
            <Tabs.Screen
                name="profile"
                options={{
                    title: 'Profile',
                    tabBarIcon: ({ color, focused }) => (
                        <TabBarIcon
                            icon={profile}
                            name="Profile"
                            focused={focused}
                            color={color}
                        />
                    )
                }}
            />
        </Tabs>
    )
}
