import Button from '@/components/Button'
import { icon } from '@/constants/Images'
import { router } from 'expo-router'
import { StatusBar } from 'expo-status-bar'
import React from 'react'
import { Image, SafeAreaView, ScrollView, Text, View } from 'react-native'

const OnBoarding = () => {
    return (
        <SafeAreaView className="">
            <ScrollView
                contentContainerStyle={{
                    height: '100%'
                }}>
                <View className="h-full px-4 w-full justify-center items-center">
                    <Image
                        source={icon}
                        resizeMode="contain"
                        className="w-[100px] h-[100px] rounded-xl"
                    />
                    <Text className=" font-medium text-lg mt-4">
                        Welcome to My App
                    </Text>

                    <Button
                        containerStyle="absolute bottom-10"
                        title="Next"
                        onPress={() => {
                            router.replace('/(tabs)')
                        }}
                    />
                </View>
            </ScrollView>
            <StatusBar style="dark" />
        </SafeAreaView>
    )
}

export default OnBoarding
